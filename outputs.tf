output "backend_bucket_id" {
  description = "Id of the created bucket s3"
  value       = aws_s3_bucket.this.id
}

output "backend_bucket_arn" {
  description = "arn of the created bucket s3"
  value       = aws_s3_bucket.this.arn
}

output "backend_bucket_name" {
  description = "name of the created bucket s3"
  value       = local.bucket_name
}

output "debug" {
  description = "For debug purpose."
  value       = local.bucket_name
}
