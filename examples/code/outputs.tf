output "backend_bucket_id" {
  description = "Id of the created bucket s3"
  value       = module.module_usage.backend_bucket_id
}

output "backend_bucket_arn" {
  description = "arn of the created bucket s3"
  value       = module.module_usage.backend_bucket_arn
}

output "backend_bucket_name" {
  description = "name of the created bucket s3"
  value       = module.module_usage.backend_bucket_name
}

output "debug" {
  description = "For debug purpose."
  value       = module.module_usage.debug
}
