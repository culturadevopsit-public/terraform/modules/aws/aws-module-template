#!/bin/bash
#******************************************************************************#
#******************************************************************************#
#******************************************************************************#
#******************************************************************************#
# PUT INTO ROOT AWS MODULE DIRECTORY ...modules/aws/publisher.sh               #
#******************************************************************************#
#******************************************************************************#
#******************************************************************************#
#******************************************************************************#

# "{.''.}>----------------------------------------------------<"
# "{.''.}>----------------------------------------------------<"
# "{.''.}> TERRAFORM GITLAB MODULE PUBLISHER                  <"
# "{.''.}>----------------------------------------------------<"
# "{.''.}> CREATED BY markitos - markitos.info@gmail.com      <"
# "{.''.}>----------------------------------------------------<"
# "{.''.}>----------------------------------------------------<"
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
TARGET_DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )/"
cd $TARGET_DIR

#******************************************************************************#
# how to use this publisher                                                    #
#******************************************************************************#
help()
{
   echo ""
   echo "{.''.}>----------------------------------------------------------------<"
   echo "{.''.}> How to publish a terraform module into gitlab private registry <"
   echo "{.''.}>----------------------------------------------------------------<"
   echo "{.''.}>"
   echo "{.''.}> Syntax: bash publish.sh PERSONAL_ACCESS_TOKEN  MODULE_VERSION PROJECT_ID DIRECTORY_NAME_ONLY"
   echo "{.''.}>"
   echo "{.''.}> PERSONAL_ACCESS_TOKEN: access_token from gitlab, ej: glpat-xxxxxxxxxxxxxxxxxxx"
   echo "{.''.}> MODULE_VERSION: major.minor.bugfix annotation, ej: 0.1.0"
   echo "{.''.}> PROJECT_ID: intener number of project/repository at gitlab, ej: 12345"
   echo "{.''.}> DIRECTORY_NAME_ONLY: from aws directory, ej: aws_canary, aws_instance, directoy name only"
   echo "{.''.}>"
   echo "{.''.}>-----------------------------------------------------------------"
   echo "{.''.}>"
   echo "{.''.}> what happen:"
   echo "{.''.}> 1. compress the module"
   echo "{.''.}> 2. public the module with version and module name from parameters"
   echo "{.''.}>"
   echo "{.''.}>-----------------------------------------------------------------"
   echo ""
}
################################################################################
# Process the input options. Add options as needed.                            #
################################################################################
# Get the options
if [ $# != 4 ]
  then
   echo ""
   echo "{.''.}>********************************************************************"
   echo "{.''.}> NO ARGUMENTS SUPPLIED, NEED TO SPECIFY ARGUMENTS FOR THE COMMAND !!"
   echo "{.''.}>********************************************************************"
   echo ""
   help
   exit 1
fi
################################################################################
# set the input options into vars and debug from stdout                        #
################################################################################
PERSONAL_ACCESS_TOKEN=$1
MODULE_VERSION=$2
PROJECT_ID=$3
DIRECTORY_NAME_ONLY=$4
MODULE_NAME=`echo ${DIRECTORY_NAME_ONLY}|sed 's/_/\-/g'|sed 's:/*$::'`
MODULE_PLATFORM=aws
MODULE_VERSION=$MODULE_VERSION
PERSONAL_ACCESS_TOKEN=$PERSONAL_ACCESS_TOKEN
PROJECT_ID=$PROJECT_ID
GITLAB_API_V4_URL="https://gitlab.com//api/v4"
GITLAB_URL="${GITLAB_API_V4_URL}/projects/${PROJECT_ID}/packages/terraform/modules/${MODULE_NAME}/${MODULE_PLATFORM}/${MODULE_VERSION}/file"
################################################################################
echo ""
echo "{.''.}>------------------------------------------------"
echo "{.''.}> PROJECT ID: ${PROJECT_ID}"
echo "{.''.}> MODULE NAME: ${MODULE_NAME}"
echo "{.''.}> MODULE PLATFORM: ${MODULE_PLATFORM}"
echo "{.''.}> MODULE NAME: ${MODULE_VERSION}"
echo "{.''.}> MODULE PATH: ${DIRECTORY_NAME_ONLY}"
echo "{.''.}> PUBLISH URL: ${GITLAB_URL}"
echo "{.''.}> ACCESS TOKEN: :P :)"
echo "{.''.}>------------------------------------------------"
read -p "{.''.}> Do you want to proceed? (yes/no) " yn
case $yn in 
	yes ) ;;
	no ) echo exiting...;
		exit;;
	* ) echo invalid response;
		exit 1;;
esac
echo "{.''.}>------------------------------------------------"
################################################################################

################################################################################
# Main program                                                                 #
################################################################################
echo ""
echo "{.''.}>----------------------------------------------------------------------------------------------------------------------------"
echo "{.''.}> Preparing the package from directory: ${DIRECTORY_NAME_ONLY}"
cd ${DIRECTORY_NAME_ONLY}
tar --exclude publish.sh --exclude .DS_Store --exclude-vcs -vczf "${TARGET_DIR}${MODULE_NAME}-${MODULE_PLATFORM}-${MODULE_VERSION}.tgz" .
cd ..
echo "{.''.}>----------------------------------------------------------------------------------------------------------------------------"

echo ""
echo "{.''.}>----------------------------------------------------------------------------------------------------------------------------"
echo "{.''.}> Publishing the package to ${GITLAB_URL}"
curl --location --header "PRIVATE-TOKEN: ${PERSONAL_ACCESS_TOKEN}" \
    --upload-file "${TARGET_DIR}${MODULE_NAME}-${MODULE_PLATFORM}-${MODULE_VERSION}.tgz" \
    ${GITLAB_URL} && \
    rm "${TARGET_DIR}${MODULE_NAME}-${MODULE_PLATFORM}-${MODULE_VERSION}.tgz" && \
    echo ""
echo "{.''.}>----------------------------------------------------------------------------------------------------------------------------"


echo "{.''.}>"
echo "{.''.}>----------------------------------------------------------------------------------------------------------------------------"
echo "{.''.}> Terraform module ${MODULE_NAME} version ${MODULE_VERSION} has been published to GitLab Infrastructure Registry"
echo "{.''.}>----------------------------------------------------------------------------------------------------------------------------"
echo ""
################################################################################


################################################################################
# HAPPY IAC :) Marco Antonio - markitos.info@gmail.com                         #
################################################################################