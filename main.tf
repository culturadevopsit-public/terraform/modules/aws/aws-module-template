locals {
  bucket_name = var.resources.bucket_name
  tags        = try(var.resources.tags != null ? var.resources.tags : null, null)

  project = var.project

  common_tags = {
    Project     = var.project.project
    Environment = var.project.environment
    CreatedBy   = var.project.createdBy
  }
}

resource "aws_s3_bucket" "this" {
  bucket = local.bucket_name

  force_destroy = true
  lifecycle {
    prevent_destroy = false
  }

  tags = merge(local.common_tags, local.tags != null ? local.tags : {
    Name        = local.bucket_name
    Description = "Created by ${local.project.createdBy}"
  })
}